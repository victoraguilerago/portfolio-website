require('dotenv').config();

module.exports = {
  serverRuntimeConfig: { // Will only be available on the server side
    PORT: '3000'
  },
  publicRuntimeConfig: { // Will be available on both server and client
    PORT: process.env.PORT
  },
  assetPrefix: process.env.NODE_ENV === 'production' ? '/{reponame}' : '',
  exportPathMap: function() {
    return {
      '/': { page: '/' }
    };
  }
}