import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
         <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <style jsx>
            {`
              body {
                margin: 0;
                max-width: 100vw;
                max-height: 100vh;
                overflow: hidden;
              }
            `}
          </style>
        </body>
      </Html>
    );
  }
}

export default MyDocument;