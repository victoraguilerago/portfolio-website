import React, { useState, useEffect } from "react"
import Circle from '../components/circle'
import Model from '../components/model'

const colors = [
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
  'rgba(74,195,255,.5)',
]

function Portfolio() {
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    setLoaded(true);
  }, [])
  return (
    <div className="portfolio">
      <div className="information">
        <div className="title">
          <span>Software</span> developer
        </div>
        <div className="name">
          Victor Aguilera
        </div>
        <div className="brief">
          The construction of solutions through digital technologies
        </div>
        <button>Contact me</button>
      </div>
      <img src="static/hologramphone.png" className="phone" />
      
      <div className="curve">
        <Circle
          limit={15}
          width={400}
          height={400}
          reduction={30}
          recursive
          color={['100', '20', '40']}
          direction={'left'}
          colors={colors}
        />
      </div>
      <div className="curvedown">
        <Circle
          limit={15}
          width={400}
          height={400}
          reduction={30}
          recursive
          color={['100', '20', '40']}
          direction={'left'}
          colors={colors}
        />
      </div>
      {
        loaded &&
        <Model modelPath={'/static/phonetoweb.glb'} controls />
      }
      <style jsx>
        {`
          .portfolio {
            font-family: 'Roboto Condensed';
            font-size: 24px;
            font-weight: bold;
            color: black;
            max-width: 100vw;
            max-height: 100vh;
            overflow: hidden;
          }
          .portfolio .information {
            padding-left: 5%;
            padding-top: 5%;
          }
          .title {
            color: black;
          }
          .title span {
            color: #2171A4;
          }
          .brief {
            max-width: 183px;
            font-weight: normal;
            font-size: 12px;
            margin-top: 12px;
          }
          button {
            width: 110px;
            height: 30px;
            background: #49B1F4;
            border-radius: 50px;
            border: none;

            font-family: 'Roboto Condensed';
            font-size: 14px;
            letter-spacing: 2px;
            font-weight: bold;
            color: white;

            margin-top: 50px;
          }
          .curve {
            position: absolute;
            width: 520px;
            height: 520px;
            top: -260px;
            right: -260px;
            z-index: -1;
            animation: grow 10s ease infinite;
          }
          .curvedown {
            position: relative;
            width: 520px;
            height: 520px;
            top: -260px;
            left: -300px;
            z-index: -1;
            animation: grow 10s ease infinite;
          }

          .phone {
            opacity: 0;
            position: relative;
            width: 100vw;
            z-index: -1;
            top: -130px;
            right: -100px;
          }
          @keyframes grow {
            0% {
              transform: scale(1);
              opacity: .9;
            }
            50% {
              transform: scale(1.04);
              opacity: .8;
            }
            100% {
              transform: scale(1);
              opacity: .9;
            }
          }
        `}
      </style>
    </div>
  )
}

export default Portfolio;