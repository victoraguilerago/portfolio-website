Sumary

(Da el resumen del issue)

Steps to reproduce

(indica los pasos para reproducir el bug)

What is the current behavior?

Which is the expected behavior?