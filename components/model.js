import React, { useRef, useState, useEffect } from "react"
import * as THREE from 'three'
import GLTFLoader from 'three-gltf-loader';

function Model(props) {
  const containerRef = useRef(null)
  const [frameId, setFrameId] = useState(null);
  const [init, setInit] = useState(null);
  const [model, setModel] = useState(null);
  const [scene, setScene] = useState(null);
  const [camera, setCamera] = useState(null);
  const [cameraControl, setCameraControl] = useState('position')
  const [renderer, setRenderer] = useState(null);



  const {
    modelPath,
    width = window.innerWidth,
    height = window.innerWidth,
    controls,
  } = props

  useEffect(() => {
    const newScene = new THREE.Scene();
    const newCamera = new THREE.PerspectiveCamera(
      100,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    );
    const loader = new GLTFLoader();

    loader.load(modelPath , (gltf) => {

      const newModel = gltf;
      
      newCamera.position.z = 1.5;
      setCamera(newCamera);

      // ADD RENDERER
      const newRenderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
      newRenderer.setClearColor(0x000000, 0); // the default
      newRenderer.setSize(width, height);
      setRenderer(newRenderer);
      
      containerRef.current.appendChild(newRenderer.domElement);
      
      // ADD MODEL
      newModel.scene.position.z = 0;
      newModel.scene.position.x = 0.8;
      newModel.scene.position.y = -0.39;
      newModel.scene.rotation.x = 0.1;
      newModel.scene.rotation.y = -1.09;
      newModel.scene.rotation.z = 0;
      newModel.scene.scale.x = 0.3;
      newModel.scene.scale.y = 0.4;
      newModel.scene.scale.z = 0.5;

      setModel(newModel);
      newScene.add(newModel.scene);

      // ADD LIGHT
      const light = new THREE.AmbientLight(0x404040, 100);
      newScene.add(light);
      setScene(newScene);
      setInit(true);
    }, undefined, (error) => {
      console.error(error);
    });
  }, [])

  const renderScene = () => {
    renderer.render(scene, camera);
  };

  const start = () => {
    if (!frameId) {
      setFrameId(requestAnimationFrame(animate));
    }
  };
  
  const animate = () => {

    renderScene();
    // frameId = window.requestAnimationFrame(animate)
    setFrameId(window.requestAnimationFrame(animate));
  };

  const loadControls = () => {
    document.addEventListener('keydown', (e) => {
      switch (e.key) {
        case 'f': {
          rotateModel('y', true);
          break;
        }
        case 't': {
          rotateModel('x', true);
          break;
        }
        case 'h': {
          rotateModel('y', false);
          break;
        }
        case 'g': {
          rotateModel('x', false);
          break;
        }
        case 'r': {
          rotateModel('z', true);
          break;
        }
        case 'y': {
          rotateModel('z', false);
          break;
        }
        case 'w': {
          translateModel('y', true);
          break;
        }
        case 'd': {
          translateModel('x', true);
          break;
        }
        case 's': {
          translateModel('y', false);
          break;
        }
        case 'a': {
          translateModel('x', false);
          break;
        }
        case 'q': {
          translateModel('z', true);
          break;
        }
        case 'e': {
          translateModel('z', false);
          break;
        }
        case 'i': {
          scaleModel('y', true);
          break;
        }
        case 'j': {
          scaleModel('x', true);
          break;
        }
        case 'k': {
          scaleModel('y', false);
          break;
        }
        case 'l': {
          scaleModel('x', false);
          break;
        }
        case 'u': {
          scaleModel('z', true);
          break;
        }
        case 'o': {
          scaleModel('z', false);
          break;
        }
        case 'i': {
          scaleModel('y', true);
          break;
        }
        case 'j': {
          scaleModel('x', true);
          break;
        }
        case 'k': {
          scaleModel('y', false);
          break;
        }
        case 'l': {
          scaleModel('x', false);
          break;
        }
        case '7': {
          moveCamara('z', true);
          break;
        }
        case '9': {
          moveCamara('z', false);
          break;
        }
        case '5': {
          moveCamara('y', true);
          break;
        }
        case '6': {
          moveCamara('x', true);
          break;
        }
        case '8': {
          moveCamara('y', false);
          break;
        }
        case '4': {
          moveCamara('x', false);
          break;
        }
        default:
          break;
      }
    });
  }

  const rotateModel = (axis, direction) => {
    console.log(model.scene);
    if (direction) {
      model.scene.rotation[axis] += 0.1;
    } else {
      model.scene.rotation[axis] -= 0.1;
    }
  };

  const translateModel = (axis, direction) => {
    console.log(model.scene);
    if (direction) {
      model.scene.position[axis] += 0.1;
    } else {
      model.scene.position[axis] -= 0.1;
    }
  };

  const scaleModel = (axis, direction) => {
    console.log(model.scene);
    if (direction) {
      model.scene.scale[axis] += 0.1;
    } else {
      model.scene.scale[axis] -= 0.1;
    }
  };
  const moveCamara = (axis, direction) => {
    console.log(camera);
    if (direction) {
      camera[cameraControl][axis] += 0.1;
    } else {
      camera[cameraControl][axis] -= 0.1;
    }
  };

  useEffect(() => {
    if (init) {
      start();
      if (controls) {
        loadControls()
      }
    }
  }, [init]);

  return (
    <div className="Model" ref={containerRef}>
      <style jsx>
        {`
          .Model {
            width: ${width}px;
            height: ${height}px;
            position: absolute;
            top: 0;
            z-index: -10;
          }
        `}
      </style>
    </div>
  );
}

export default Model;