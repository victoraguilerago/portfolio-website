import React from 'react';

function Circle (props) {
  const {
    width,
    height,
    limit,
    recursive,
    reduction,
    color,
    colors,
    direction,
  } = props;
  return (
    <div className="circle">
      {
        limit > 0 && recursive &&
        <Circle 
          limit={limit - 1}
          width={width - reduction}
          height={height - reduction}
          reduction={reduction}
          color={color}
          recursive
          colors={colors}
        />
      }
      <style jsx>
        {`
          .circle {
            position: relative;
            border-radius: 100%;
            width: ${width}px;
            height: ${height}px;

            margin: 0 auto;
            
            display: flex;
            justify-content: center;
            align-items: center;

            background: ${colors[limit]};
            box-shadow: inset 0 0 5px rgba(0,0,0,.3);
          }
        `}
      </style>
    </div>
  )
}
export default Circle;